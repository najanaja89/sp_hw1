﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Threading;

namespace sp_hw1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var processesList = Process.GetProcesses().ToList();
            taskManagerDataGrid.ItemsSource = null;
            taskManagerDataGrid.ItemsSource = processesList;

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(500);
            timer.Tick += Timer_Tick;

            timer.Start();
        }

        private void Timer_Tick(object sender, System.EventArgs e)
        {
            var processesList = Process.GetProcesses().ToList();
            taskManagerDataGrid.ItemsSource = null;
            taskManagerDataGrid.Items.Refresh();
            taskManagerDataGrid.ItemsSource = processesList;
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var currentProcess = (Process)taskManagerDataGrid.SelectedItem;
                var listProcess = Process.GetProcessesByName(currentProcess.ProcessName);
                foreach (var process in listProcess)
                {
                    process.Kill();
                }
                var processesList = Process.GetProcesses().ToList();
                taskManagerDataGrid.ItemsSource = null;
                taskManagerDataGrid.Items.Refresh();
                taskManagerDataGrid.ItemsSource = processesList;

            }
            catch (Exception ex)
            {

                MessageBox.Show("Access Denied");
                return;
            }
        }
    }
}

